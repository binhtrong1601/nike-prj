import {
  faCartShopping,
  faHeart,
  faMagnifyingGlass,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import ItemTitle from "../ItemTitle";
import "./styles.css";

const Header = () => {
  return (
    <div className="header">
      <div className="header-container">
        <div className="logo">
          <img
            style={{ maxWidth: 60 }}
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Logo_NIKE.svg/1200px-Logo_NIKE.svg.png"
          />
        </div>
        <div className="menu">
          <ItemTitle title={"Men"}>
            <div>Test</div>
          </ItemTitle>
          <ItemTitle title={"Shoes"}>
            <div>Test1</div>
          </ItemTitle>
          <ItemTitle title={"Shoes"}>
            <div>Test2</div>
          </ItemTitle>
          <ItemTitle title={"Shoes"}>
            <div>Test3</div>
          </ItemTitle>
        </div>
        <div className="box-search">
          <div className="search">
            <div className="search-btn">
              <input
                type="search"
                className="search-input"
                placeholder="Search"
              />
              <div className="search-icon">
                <FontAwesomeIcon icon={faMagnifyingGlass} />
              </div>
            </div>
          </div>
          <div className="icon">
            <div className="icon-item">
              <FontAwesomeIcon icon={faHeart} size="xl" />
            </div>
            <div className="icon-item">
              <FontAwesomeIcon icon={faCartShopping} size="xl" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
