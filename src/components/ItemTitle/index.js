import { useState } from "react";
import "./styles.css";

const ItemTitle = ({ children, title }) => {
  const [showDropdown, setShowDropdown] = useState(false);

  return (
    <div className="menu-item">
      <div className="dropdown">
        <div
          className="dropbtn"
          onMouseEnter={() => setShowDropdown(true)}
          onMouseLeave={() => setShowDropdown(false)}
        >
          {title}
        </div>
        {showDropdown === true && (
          <div
            className="dropdown-content"
            onMouseEnter={() => setShowDropdown(true)}
            onMouseLeave={() => setShowDropdown(false)}
          >
            {children}
          </div>
        )}
      </div>
    </div>
  );
};
export default ItemTitle;
